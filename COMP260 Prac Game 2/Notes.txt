
---  Demonstrate Your Work  ---

You would use transform when there isn't any collisions happening, and rigidbody
also makes it easier to apply force to an object and have it interact.

Using rigidbody to move the paddle let me hit the walls and not have the player
glitch in to it.

I tried MovePosition, and found that it wasn't hitting the puck at a force and
appearing to just dribble it. 

I then tried Position, and the puck was just glitching all over the place.

I tried AddForce second, and it was making the puck seem a little laggy,
as when I would try to turn around it would first slow down before changing
direction. However, it was hitting the puck with force which was a good sign.

I then tried velocity, and it gave me the movement desired after some tweaking
with the speed variable, and was also hitting it with force.
