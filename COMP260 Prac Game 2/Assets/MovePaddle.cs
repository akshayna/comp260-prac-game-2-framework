﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	
	private Rigidbody rigidbody;
	private float speed = 7f;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate (){

		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		rigidbody.velocity = new Vector3 (moveHorizontal *speed  , 0f, moveVertical*speed );

		//Vector3 movement = new Vector3 (moveHorizontal * speed * Time.deltaTime  , 0f, moveVertical * speed * Time.deltaTime);
		//rigidbody.MovePosition ( transform.position + movement);
}



//	void FixedUpdate () {
//		Vector3 pos = GetMousePosition();
//		Vector3 dir = pos - rigidbody.position;
//		Vector3 vel = dir.normalized * speed;
//
//		// check is this speed is going to overshoot the target
//		float move = speed * Time.fixedDeltaTime;
//		float distToTarget = dir.magnitude;
//
//		if (move > distToTarget) {
//			// scale the velocity down appropriately
//			vel = vel * distToTarget / move;
//		}
//
//		rigidbody.velocity = vel;
//	}
//
//	private Vector3 GetMousePosition() {
//		// create a ray from the camera 
//		// passing through the mouse position
//		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//
//		// find out where the ray intersects the XZ plane
//		Plane plane = new Plane(Vector3.up, Vector3.zero);
//		float distance = 0;
//		plane.Raycast(ray, out distance);
//		return ray.GetPoint(distance);
//	}
//
//	void OnDrawGizmos() {
//		// draw the mouse ray
//		Gizmos.color = Color.yellow;
//		Vector3 pos = GetMousePosition();
//		Gizmos.DrawLine(Camera.main.transform.position, pos);
//	}



}
